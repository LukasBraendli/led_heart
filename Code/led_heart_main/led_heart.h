/**
 * Author: Lukas Brändli
 * 
 * This file contains pin definitions etc. for 
 * the led_heart project. 
 * The pin names are conform with the
 * Arduino Pro Mini.
 *
*/

#ifndef LED_HEART_H
#define LED_HEART_H

/* ========== PIN DEFINITIONS ========== */
/* ----- TOUCH INPUTS ----- */
#define TOUCH1_PIN  A0
#define TOUCH2_PIN  A1
#define TOUCH3_PIN  A2
#define TOUCH_AUX1_PIN  A6
#define TOUCH_AUX2_PIN  A7
#define GROUNDED_PIN  -1  // internal (see ADCTouchSensor library)

/* ----- NEOPIXEL ----- */
#define PIXELS_FRONT_PIN   6
#define PIXELS_SIDE_PIN    5

/* ----- ACCELEROMETER ----- */
#define ACCEL_SDA_PIN   A4
#define ACCEL_SCL_PIN   A5
#define ACCEL_INT1_PIN  2
#define ACCEL_INT2_PIN  3

/* ----- POWER CONTROL ----- */
#define PWR_EN_PIN  8
#define PWR_SENSE_PIN   7
#define USB_SENSE_PIN   4
#define BAT_SENSE_PIN   A3
#define BAT_VOLT_DIV_RATIO 0.713F
#define MAX_ADC_READ  1023U // @3.3V
#define BAT_FULL_VOLT 4.2F
#define BAT_EMPTY_VOLT  3.5F
#define BAT_FULL_ADC_READ 781U
#define BAT_EMPTY_ADC_READ  651U
#define BAT_ADC_RANGE 130

/* ----- ADDITIONAL I/O ----- */
#define IO1 9
#define IO2 10


/* ========== NEOPIXEL ========== */
/* ----- FRONT PIXELS ----- */
#define PIXELS_FRONT_NUM    17
#define PIXELS_FRONT_RIGHT_NUM  9
#define PIXELS_FRONT_LEFT_NUM  8
/* ----- SIDE PIXELS ----- */
#define PIXELS_SIDE_NUM    4

const uint8_t xyPixelMap_front[PIXELS_FRONT_NUM] = {};


/* ========== TOUCH HANDLING ========== */
#define TOUCH_THRESHOLD 100
#define TOUCH_THRESHOLD_ANALOG  500 // threshold for pure analog pins (A6 & A7)
// defines for touch1 & touch3
#define TOUCH13_DEBOUNCE_TIMEOUT 0UL
#define TOUCH13_CLICK_TIMEOUT 0UL
#define TOUCH13_HOLD_TIMEOUT 0UL
#define TOUCH13_LONG_HOLD_TIMEOUT 0UL
#define TOUCH13_HOLD_REPEAT_PERIOD 20UL
#define TOUCH13_LONG_HOLD_REPEAT_PERIOD 20UL
// defines for touch2
#define TOUCH2_DEBOUNCE_TIMEOUT 0UL
#define TOUCH2_CLICK_TIMEOUT 0UL
#define TOUCH2_HOLD_TIMEOUT 0UL
#define TOUCH2_LONG_HOLD_TIMEOUT 0UL
#define TOUCH2_HOLD_REPEAT_PERIOD 20UL
#define TOUCH2_LONG_HOLD_REPEAT_PERIOD 20UL

/* ========== KIND OF LIKE ... YOU KNOW ... KIND OF LIKE EVERYTHING AND STUFF ========== */
// enumerate physical inputs (Button + used touch inputs)
enum Inputs{
  button_id, // first entry is default mode
  touch1_id,
  touch2_id,
  touch3_id,
  NUMINPUTS // hack to know the number of modes in enum "Inputs"
};

#endif  /* LED_HEART_H */

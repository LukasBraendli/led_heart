#ifndef Mode_h
#define Mode_h


#include "Arduino.h"
#include "led_heart.h"
#include <FastLED.h>  // neopixels driver
#include <ADCTouchSensor.h> // touch sensing using ADC inputs
#include <Adafruit_Sensor.h>  // accelerometer driver
#include <SmartButton.h>  // handles buttons (click, double-click, repeat-on-long-press etc.)
using namespace smartbutton;

/* ========== ABSTRACT CLASS ========== */
class Mode
{
public:
  Mode(CRGB *_pixelsFront, CRGB *_pixelsSide);
  virtual void start(void){};
  virtual void stop(void){};
  virtual void update(void){};
  virtual void buttonHandler(SmartButton *button, SmartButton::Event event, int clickCounter){};
  virtual void touch1Handler(SmartButton *button, SmartButton::Event event, int clickCounter){};
  virtual void touch2Handler(SmartButton *button, SmartButton::Event event, int clickCounter){};
  virtual void touch3Handler(SmartButton *button, SmartButton::Event event, int clickCounter){};
  
protected:
  CRGB *pixelsFront;
  CRGB *pixelsSide;
};

enum Modes{
  batteryStatus,
  street,
  userColor,
  //userColorChase,
  rainbow,
  NUMMODES  // "hack" to know the number of modes in enum "Modes"
};

/* ========== DERIVED CLASSES ========== */

/* ----- battery status mode ----- */
class BatteryStatus_Mode : public Mode{
public:
  BatteryStatus_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide);
  void start(void);
  void update(void);
private:
  static const uint8_t brightness = 20;
  static const uint16_t avgCount = 100;
  uint8_t numLEDsOn[avgCount];
  uint16_t currentIndex;
};

/* ----- street mode ----- */
class Street_Mode:public Mode{
public:
  Street_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide);
  void start(void);
  void update(void);
  void touch1Handler(SmartButton *button, SmartButton::Event event, int clickCounter);
  void touch2Handler(SmartButton *button, SmartButton::Event event, int clickCounter);
  void touch3Handler(SmartButton *button, SmartButton::Event event, int clickCounter);

private:
  enum Effects{
    blinkfade,
    constant,
    circle,
    NUMEFFECTS
  };
  uint8_t currentEffect;
  uint8_t blinkCycle;
  const uint8_t blinkSpeed = 255;
  const uint8_t circleSpeed = 200; // 255 --> fast, 0 --> slow
  uint32_t lastTime;
};

/* ----- custom user color mode ----- */
class UserColor_Mode:public Mode{
public:
  UserColor_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide);
  void update(void);
  void touch1Handler(SmartButton *button, SmartButton::Event event, int clickCounter);
  void touch2Handler(SmartButton *button, SmartButton::Event event, int clickCounter);
  void touch3Handler(SmartButton *button, SmartButton::Event event, int clickCounter);

private:
  uint8_t hue = 0;
};

/* ----- rainbow ----- */
class Rainbow_Mode:public Mode{
public:
  Rainbow_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide);
  void start(void);
  void update(void);
  void touch1Handler(SmartButton *button, SmartButton::Event event, int clickCounter);
  void touch2Handler(SmartButton *button, SmartButton::Event event, int clickCounter);
  void touch3Handler(SmartButton *button, SmartButton::Event event, int clickCounter);

private:
  enum Effects{
    circle,
    blinkfade,
    NUMEFFECTS
  };
  uint8_t currentEffect;
  uint8_t blinkCycle;
  const uint8_t blinkSpeed = 255;
  const uint8_t circleSpeed = 200; // 255 --> fast, 0 --> slow
  uint16_t sPseudotime;
  uint16_t sLastMillis;
  uint16_t sHue16;
};


extern void decreaseBrightness(uint8_t deltab);

extern void increaseBrightness(uint8_t deltab);

extern CHSV cycleHSV(uint8_t hstep, uint8_t brightness);

extern uint8_t attackDecayWave8(uint8_t i, uint8_t attackSpeed);

extern void adaptSideToFront(void);

#endif

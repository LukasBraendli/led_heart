#include "Arduino.h"
#include "Mode.h"

/* ========== MODE ========== */

/* ----- constructor ----- */
Mode::Mode(CRGB *_pixelsFront, CRGB *_pixelsSide)
  : pixelsFront(_pixelsFront), pixelsSide(_pixelsSide){}


/* ========== BATTERY STATUS ========== */

/* ----- constructor ----- */
BatteryStatus_Mode::BatteryStatus_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide)
        : Mode(_pixelsFront, _pixelsSide){}

void BatteryStatus_Mode::start(void){
  FastLED.setBrightness(brightness);
  for(int i = 0; i < avgCount; i++){
    numLEDsOn[i] = 0;
  }
  currentIndex = 0;
}
/* ----- update() -----*/
void BatteryStatus_Mode::update(){
  uint16_t batADCRead = analogRead(BAT_SENSE_PIN);
  //batADCRead = constrain(batADCRead, BAT_EMPTY_ADC_READ, BAT_FULL_ADC_READ);
  float batVolt = 3.3/1024.f*float(batADCRead)/BAT_VOLT_DIV_RATIO;
#ifdef DEBUG
  Serial.println(batVolt);
#endif
  //numLEDsOn[currentIndex] = map(batADCRead, BAT_EMPTY_ADC_READ, BAT_FULL_ADC_READ, 1, 9);
  numLEDsOn[currentIndex] = uint8_t((constrain(batVolt, BAT_EMPTY_VOLT, BAT_FULL_VOLT)-BAT_EMPTY_VOLT)/(BAT_FULL_VOLT-BAT_EMPTY_VOLT)*(9.f-1.f)+1.f);
  currentIndex = ++currentIndex >= avgCount ? 0 : currentIndex;
  uint16_t numLEDsOnAvg = 0;
  for(int i = 0; i < avgCount; i++){
    numLEDsOnAvg += numLEDsOn[i];
  }
  numLEDsOnAvg = numLEDsOnAvg / avgCount;

  fill_solid(pixelsFront, numLEDsOnAvg, CRGB::Green);
  fill_solid(pixelsFront + numLEDsOnAvg, (PIXELS_FRONT_RIGHT_NUM - numLEDsOnAvg)*2, CRGB::Red);
  fill_solid(pixelsFront + numLEDsOnAvg + (PIXELS_FRONT_RIGHT_NUM - numLEDsOnAvg)*2, numLEDsOnAvg - 1,
              CRGB::Green);

  fill_solid(pixelsSide, PIXELS_SIDE_NUM, CRGB::Orange);
}


/* ========== STREET ========== */
Street_Mode::Street_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide)
        : Mode(_pixelsFront, _pixelsSide){}

void Street_Mode::start(){
  currentEffect = 0;
  blinkCycle = 0;
  lastTime = millis();
}

void Street_Mode::update(){
  switch(currentEffect){
    case constant:
      fill_solid(pixelsFront, PIXELS_FRONT_NUM, CHSV(0, 0, 255)); // white
      fill_solid(pixelsSide, PIXELS_SIDE_NUM, CHSV(50, 255, 255));  // orange
    break;
    case blinkfade:
      if(lastTime + (255 - blinkSpeed) < millis()){
        fill_solid(pixelsFront, PIXELS_FRONT_NUM, CHSV(0, 0, attackDecayWave8(blinkCycle++, 3))); // white
        fill_solid(pixelsSide, PIXELS_SIDE_NUM, CHSV(50, 255, attackDecayWave8(blinkCycle++, 3)));  // orange
        lastTime = millis();
      }
    break;
    case circle:
      if(lastTime + (255 - circleSpeed) < millis()){
        for(int i = 0; i < PIXELS_FRONT_NUM; i++){
          pixelsFront[i] = CHSV(0, 0, attackDecayWave8(i*255/PIXELS_FRONT_NUM + blinkCycle++, 3));
          //pixelsFront[i] = CHSV(0, 0, i*255/PIXELS_FRONT_NUM + blinkCycle--); // gives sharper animation
        }
        lastTime = millis();
      }
    break;
  }
}

/*void UserColor_Mode::buttonHandler(SmartButton *button, SmartButton::Event event, int clickCounter){}*/
void Street_Mode::touch1Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
  event == SmartButton::Event::HOLD_REPEAT ||
  event == SmartButton::Event::LONG_HOLD ||
  event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    decreaseBrightness(1);
  }
}

void Street_Mode::touch2Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::LONG_HOLD)
  {
    currentEffect++;
    if(currentEffect >= Effects::NUMEFFECTS){
      currentEffect = 0;
    }
  }
}

void Street_Mode::touch3Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
  event == SmartButton::Event::HOLD_REPEAT ||
  event == SmartButton::Event::LONG_HOLD ||
  event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    increaseBrightness(1);
  }
}


/* ========== USER COLOR ========== */

/* ----- constructor ----- */
UserColor_Mode::UserColor_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide)
        : Mode(_pixelsFront, _pixelsSide){}

void UserColor_Mode::update(){
  CHSV color = CHSV(hue, 255, 255);
  fill_solid(pixelsFront, PIXELS_FRONT_NUM, color);
  fill_solid(pixelsSide, PIXELS_SIDE_NUM, color);
}

/*void UserColor_Mode::buttonHandler(SmartButton *button, SmartButton::Event event, int clickCounter){}*/
void UserColor_Mode::touch1Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
  event == SmartButton::Event::HOLD_REPEAT ||
  event == SmartButton::Event::LONG_HOLD ||
  event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    decreaseBrightness(1);
  }
}

void UserColor_Mode::touch2Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
  event == SmartButton::Event::HOLD_REPEAT ||
  event == SmartButton::Event::LONG_HOLD ||
  event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    hue++;
  }
}

void UserColor_Mode::touch3Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
  event == SmartButton::Event::HOLD_REPEAT ||
  event == SmartButton::Event::LONG_HOLD ||
  event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    increaseBrightness(1);
  }
}


/* ========== RAINBOW ========== */
Rainbow_Mode::Rainbow_Mode(CRGB *_pixelsFront, CRGB *_pixelsSide)
        : Mode(_pixelsFront, _pixelsSide){}

void Rainbow_Mode::start(){
  currentEffect = 0;
  blinkCycle = 0;
  
  sPseudotime = 0;
  sLastMillis = 0;
  sHue16 = 0;
}

void Rainbow_Mode::update(){
  uint8_t sat8 = beatsin88( 87, 220, 250);
  uint8_t brightdepth = beatsin88( 341, 96, 224);
  uint16_t brightnessthetainc16 = beatsin88( 203, (25 * 256), (40 * 256));
  uint8_t msmultiplier = beatsin88(147, 23, 60);

  uint16_t hue16 = sHue16;//gHue * 256;
  uint16_t hueinc16 = beatsin88(113, 1, 3000);
  
  uint16_t ms = millis();
  uint16_t deltams = ms - sLastMillis ;
  sLastMillis  = ms;
  sPseudotime += deltams * msmultiplier;
  sHue16 += deltams * beatsin88( 400, 5,9);
  uint16_t brightnesstheta16 = sPseudotime;

  switch(currentEffect){
    case circle:
      for( uint16_t i = 0 ; i < PIXELS_FRONT_NUM; i++) {
        hue16 += hueinc16;
        uint8_t hue8 = hue16 / 256;
    
        brightnesstheta16  += brightnessthetainc16;
        uint16_t b16 = sin16( brightnesstheta16  ) + 32768;
    
        uint16_t bri16 = (uint32_t)((uint32_t)b16 * (uint32_t)b16) / 65536;
        uint8_t bri8 = (uint32_t)(((uint32_t)bri16) * brightdepth) / 65536;
        bri8 += (255 - brightdepth);
        
        CRGB newcolor = CHSV( hue8, sat8, bri8);
        
        uint16_t pixelnumber = i;
        pixelnumber = (PIXELS_FRONT_NUM-1) - pixelnumber;
        
        nblend( pixelsFront[pixelnumber], newcolor, 64);
      }
    
    break;
    case blinkfade:
      hue16 += hueinc16;
      uint8_t hue8 = hue16 / 256;
  
      brightnesstheta16  += brightnessthetainc16;
      uint16_t b16 = sin16( brightnesstheta16  ) + 32768;
  
      uint16_t bri16 = (uint32_t)((uint32_t)b16 * (uint32_t)b16) / 65536;
      uint8_t bri8 = (uint32_t)(((uint32_t)bri16) * brightdepth) / 65536;
      bri8 += (255 - brightdepth);
      
      CRGB newcolor = CHSV( hue8, sat8, bri8);

      nblend( pixelsFront[0], newcolor, 64);
      for(uint16_t i = 1; i < PIXELS_FRONT_NUM; i++){
        pixelsFront[i] = pixelsFront[0];
      }
    break;
  }

  // change sidefacing leds according to frontfacing ones
  adaptSideToFront();
}

void Rainbow_Mode::touch1Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
  event == SmartButton::Event::HOLD_REPEAT ||
  event == SmartButton::Event::LONG_HOLD ||
  event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    decreaseBrightness(1);
  }
}

void Rainbow_Mode::touch2Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::LONG_HOLD)
  {
    currentEffect++;
    if(currentEffect >= Effects::NUMEFFECTS){
      currentEffect = 0;
    }
  }
}

void Rainbow_Mode::touch3Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
  event == SmartButton::Event::HOLD_REPEAT ||
  event == SmartButton::Event::LONG_HOLD ||
  event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    increaseBrightness(1);
  }
}

#pragma once

#ifndef COLORS_H
#define COLORS_H

#define FUSE_RGB(r, g, b)	(((uint32_t)r << 16) | ((uint32_t)g << 8) | b)

#define BLACK		FUSE_RGB(0, 0, 0)
#define WHITE		FUSE_RGB(255, 255, 255)
#define LIGHTGRAY	FUSE_RGB(195, 195, 195)
#define DARKGRAY	FUSE_RGB(127, 127, 127)
#define PINK		FUSE_RGB(255, 174, 201)
#define RED			FUSE_RGB(255, 0, 0)
#define ORANGE		FUSE_RGB(255, 153, 0)
#define MAROON		FUSE_RGB(136, 0, 21)
#define MAGENTA		FUSE_RGB(255, 0, 255)
#define BROWN		FUSE_RGB(185, 122, 87)
#define YELLOW		FUSE_RGB(255, 255, 0)
#define LIGHTGREEN	FUSE_RGB(128, 255, 128)
#define GREEN		FUSE_RGB(0, 255, 0)
#define DARKGREEN	FUSE_RGB(0, 128, 0)
#define LIGHTBLUE	FUSE_RGB(128, 128, 255)
#define BLUE		FUSE_RGB(0, 0, 255)
#define DARKBLUE	FUSE_RGB(0, 0, 128)
#define PURPLE		FUSE_RGB(163, 73, 164)

// gamma correction for ws2812 type rgb leds
// how to use the array:
//	-	take the r value of the color in rgb format and replace it with the 
//		value at gamma_correction[r]
//	-	repeat for g and b
/*byte gamma_correction[] = 
{
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
	0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
	1,  1,  1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  2,  2,  2,
	2,  3,  3,  3,  3,  3,  3,  3,  4,  4,  4,  4,  4,  5,  5,  5,
	5,  6,  6,  6,  6,  7,  7,  7,  7,  8,  8,  8,  9,  9,  9, 10,
	10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
	17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
	25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
	37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
	51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
	69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
	90, 92, 93, 95, 96, 98, 99,101,102,104,105,107,109,110,112,114,
	115,117,119,120,122,124,126,127,129,131,133,135,137,138,140,142,
	144,146,148,150,152,154,156,158,160,162,164,167,169,171,173,175,
	177,180,182,184,186,189,191,193,196,198,200,203,205,208,210,213,
	215,218,220,223,225,228,231,233,236,239,241,244,247,249,252,255
};
*/
#endif
/**
 * Author: Lukas Brändli
 * 
 * This is the main file for the led_heart project.
 */

/* ========== INCLUDES ========== */

/* ----- LIBRARIES ----- */
#include <FastLED.h>  // neopixels driver
#include <ADCTouchSensor.h> // touch sensing using ADC inputs
#include <Adafruit_Sensor.h>  // accelerometer driver
#include <SmartButton.h>  // handles buttons (click, double-click, repeat-on-long-press etc.)
#include "Mode.h"
using namespace smartbutton;

/* ----- OTHER ----- */
#include "led_heart.h"  // pin definitions
#include "Colors.h" // most used colors as Color(r,g,b) type (see Adafruit_NeoPixel library)


/* ========== DEBUG ========== */
//#define DEBUG

/* ========== VARIABLES ==========*/

/* ----- SYSTEM ----- */
#define STARTUP_SHUTDOWN_ANIMATION_SPEED  65  // animation speed from 0-100!
#define STARTUP_SHUTDOWN_ANIMATION_DELAY  100 - STARTUP_SHUTDOWN_ANIMATION_SPEED
#define STARTUP_SHUTDOWN_BRIGHTNESS 20

/* ----- TOUCH SENSORS-----*/
ADCTouchSensor touch1 = ADCTouchSensor(TOUCH1_PIN, GROUNDED_PIN); 
ADCTouchSensor touch2 = ADCTouchSensor(TOUCH2_PIN, GROUNDED_PIN); 
ADCTouchSensor touch3 = ADCTouchSensor(TOUCH3_PIN, GROUNDED_PIN);

bool isTouched1(SmartButton *button){
  return (touch1.read() > TOUCH_THRESHOLD);
}

bool isTouched2(SmartButton *button){
  return (touch2.read() > TOUCH_THRESHOLD);
}

bool isTouched3(SmartButton *button){
  return (touch3.read() > TOUCH_THRESHOLD);
}

SmartButton button(PWR_SENSE_PIN, SmartButton::InputType::NORMAL_LOW);
SmartButton touch1_b(-1,
                      SmartButton::InputType::NORMAL_HIGH,
                      isTouched1,
                      NULL,
                      NULL,
                      TOUCH13_DEBOUNCE_TIMEOUT,
                      TOUCH13_CLICK_TIMEOUT,
                      TOUCH13_HOLD_TIMEOUT,
                      TOUCH13_LONG_HOLD_TIMEOUT,
                      TOUCH13_HOLD_REPEAT_PERIOD,
                      TOUCH13_LONG_HOLD_REPEAT_PERIOD);
SmartButton touch2_b(-1,
                      SmartButton::InputType::NORMAL_HIGH,
                      isTouched2,
                      NULL,
                      NULL,
                      TOUCH13_DEBOUNCE_TIMEOUT,
                      TOUCH13_CLICK_TIMEOUT,
                      TOUCH13_HOLD_TIMEOUT,
                      TOUCH13_LONG_HOLD_TIMEOUT,
                      TOUCH13_HOLD_REPEAT_PERIOD,
                      TOUCH13_LONG_HOLD_REPEAT_PERIOD);
SmartButton touch3_b(-1,
                      SmartButton::InputType::NORMAL_HIGH,
                      isTouched3,
                      NULL,
                      NULL,
                      TOUCH13_DEBOUNCE_TIMEOUT,
                      TOUCH13_CLICK_TIMEOUT,
                      TOUCH13_HOLD_TIMEOUT,
                      TOUCH13_LONG_HOLD_TIMEOUT,
                      TOUCH13_HOLD_REPEAT_PERIOD,
                      TOUCH13_LONG_HOLD_REPEAT_PERIOD);

/* ----- MODES ----- */
int currentMode = 0;

/* ----- MODE INSTANCES ----- */
Mode *modeAllocation[Modes::NUMMODES];

/* ----- NEOPIXELS ----- */
// allocate memory for pixel data
CRGB pixelsFront[PIXELS_FRONT_NUM];
CRGB pixelsSide[PIXELS_SIDE_NUM];

#define MIN_BRIGHTNESS  0
#define MAX_BRIGHTNESS  150
uint8_t gBrightness = STARTUP_SHUTDOWN_BRIGHTNESS;
uint8_t n = 0;

/* ========== SETUP ===== */
void setup(){
#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("\n\n==============================\n\n");
#endif
  
  pinSetup();

  neoPixelSetup();

  touchSetup();

  smartButtonSetup();

  modesSetup();

  startup();
}

/* ========== MAIN LOOP ===== */
void loop() {
  SmartButton::service();
  modeAllocation[currentMode]->update();
  FastLED.show();
}

/* ========== SYSTEM FUNCTIONS ========== */

/**
 * Initializes pins (input, output, pullup, analog)
 */
void pinSetup(){
#ifdef DEBUG
  Serial.println("----- PIN SETUP -----");
#endif
  // power enable pin setup
  pinMode(PWR_EN_PIN, OUTPUT);
  // power button pin
  pinMode(PWR_SENSE_PIN, INPUT);
  // touch pins
  pinMode(TOUCH1_PIN, INPUT); // analog
  pinMode(TOUCH2_PIN, INPUT); // analog
  pinMode(TOUCH3_PIN, INPUT); // analog
  pinMode(TOUCH_AUX1_PIN, INPUT); // analog
  pinMode(TOUCH_AUX2_PIN, INPUT); // analog
}

/**
 * Initializes capacitive touch inputs
 */
void touchSetup(){
#ifdef DEBUG
  Serial.println("----- TOUCH SETUP -----");
#endif
  touch1.begin();
  touch2.begin();
  touch3.begin();
}

/**
 * Starts smartbuttons
 */
void smartButtonSetup(){
  button.begin(modeHandler);
  touch1_b.begin(touch1_Handler);
  touch2_b.begin(touch2_Handler);
  touch3_b.begin(touch3_Handler);
}

/**
 * This function handles the physical startup procedure. 
 * This involves:
 * - enabling the power supply (3.3V for logic)
 * - providing visual startup/test sequence with LEDs
 */
void startup(){
#ifdef DEBUG
  Serial.println("----- STARTUP -----");
#endif
  digitalWrite(PWR_EN_PIN, HIGH); // write pin high enable 3.3v supply
  FastLED.setBrightness(STARTUP_SHUTDOWN_BRIGHTNESS);
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CRGB::Red);
  for(int p = 0; p < PIXELS_FRONT_NUM; p++){
    pixelsFront[p] = CRGB::Red;
    FastLED.show();
    delay(STARTUP_SHUTDOWN_ANIMATION_DELAY);
  }
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CRGB::Blue);
  for(int p = 0; p < PIXELS_FRONT_NUM; p++){
    pixelsFront[p] = CRGB::Blue;
    FastLED.show();
    delay(STARTUP_SHUTDOWN_ANIMATION_DELAY);
  }
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CRGB::Green);
  for(int p = 0; p < PIXELS_FRONT_NUM; p++){
    pixelsFront[p] = CRGB::Green;
    FastLED.show();
    delay(STARTUP_SHUTDOWN_ANIMATION_DELAY);
  }
}

/**
 * This function handles the physical shutdown procedure. 
 * This involves:
 * - providing visual shutdown/test sequence with LEDs
 * - disabling the power supply (3.3V for logic)
 */
void shutdown(){
#ifdef DEBUG
  Serial.println("----- SHUTDOWN -----");
#endif
  FastLED.setBrightness(STARTUP_SHUTDOWN_BRIGHTNESS);
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CRGB::Green);
  fill_solid(&pixelsFront[0], PIXELS_FRONT_NUM, CRGB::Green);
  FastLED.show();
  delay(400);
  for(int p = PIXELS_FRONT_NUM - 1; p >= 0; p--){
    pixelsFront[p] = CRGB::Blue;
    FastLED.show();
    delay(STARTUP_SHUTDOWN_ANIMATION_DELAY);
  }
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CRGB::Blue);
  for(int p = PIXELS_FRONT_NUM - 1; p >= 0; p--){
    pixelsFront[p] = CRGB::Red;
    FastLED.show();
    delay(STARTUP_SHUTDOWN_ANIMATION_DELAY);
  }
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CRGB::Red);
  for(int p = PIXELS_FRONT_NUM - 1; p >= 0; p--){
    pixelsFront[p] = CRGB::Black;
    FastLED.show();
    delay(STARTUP_SHUTDOWN_ANIMATION_DELAY);
  }
  // !!! ensure all pixels are off !!!
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CRGB::Black);
  fill_solid(&pixelsFront[0], PIXELS_FRONT_NUM, CRGB::Black);
  FastLED.show();
  digitalWrite(PWR_EN_PIN, LOW); // write pin low on shutdown to disable 3.3v supply
  while(1){} // infinite loop in case user continues to press button or button is jammed
}

/**
 * Initializes the modes array
 */
void modesSetup(){
  modeAllocation[Modes::batteryStatus] = new BatteryStatus_Mode(pixelsFront, pixelsSide);
  modeAllocation[Modes::street] = new Street_Mode(pixelsFront, pixelsSide);
  modeAllocation[Modes::userColor] = new UserColor_Mode(pixelsFront, pixelsSide);
  modeAllocation[Modes::rainbow] = new Rainbow_Mode(pixelsFront, pixelsSide);

  modeAllocation[currentMode]->start();
}

/**
 * Switch from the current mode to the next. 
 * This also includes changing the button handlers. 
 */
void nextMode(){
  //modeAllocation[currentMode]->stop();
  
  // set new mode
  currentMode = (currentMode + 1);
  if(currentMode >= Modes::NUMMODES){
    currentMode = 0;
  }

  modeAllocation[currentMode]->start();
}



/**
 * Checks if a capacitive touch input has been activated
 * takes:
 * - "touch" an instance of ADCTouchSensor
 * - "thresholdAbs" the absolute threshold that has to be reached 
 *    so the input counts as activated"
 * returns:
 * - true iff "touch" measures an input above "thresholdAbs"
 * - false else
 */
bool isTouched(ADCTouchSensor *touch, int thresholdAbs){
  return (touch->read() > thresholdAbs);
}

/**
 * Checks if a capacitive touch input has been activated with a certain 
 * time period from the last registered touch
 * takes:
 * - "touch" an instance of ADCTouchSensor
 * - "thresholdAbs" the absolute threshold that has to be reached 
 *    so the input counts as activated"
 * returns:
 * - true iff "touch" measures an input above "thresholdAbs"
 * - false else
 */


 /* ========== NEOPIXEL FUNCTIONS ========== */
void neoPixelSetup(){
#ifdef DEBUG
  Serial.println("----- NEOPIXEL SETUP -----");
#endif
  // set up FastLed library instance
  FastLED.addLeds<NEOPIXEL, PIXELS_FRONT_PIN>(pixelsFront, PIXELS_FRONT_NUM);
  FastLED.addLeds<NEOPIXEL, PIXELS_SIDE_PIN>(pixelsSide, PIXELS_SIDE_NUM);
}

/**
 * Dims all the pixels in the given range by "val"/256 ("val" 256th). 
 * Does NOT fade to black
 * takes:
 * - "pixel" pointer to the first pixel in the pixel array that should be dimmed
 * - "num" number of pixels from "pixel" (including) that should be dimmed
 * - "val" the new brightness as percentage of the current brightness in 256ths
 */
void dim(CRGB *pixel, int num, uint8_t val){
  for(int p = 0; p < num; p++){
    pixel[p].nscale8(val);
  }
}

/**
 * Sets brightness of all pixels int the given range to specified brightness value
 * takes:
 * - "pixel" pointer to the first pixel in the pixel array that should be dimmed
 * - "num" number of pixels from "pixel" (including) that should be set
 * - "b" brightness value in the range 0-255, 0 = off, 255 = max
 */
void setBrightnessBulk(CRGB *pixel, int num, uint8_t b){
  for(int p = 0; p < num; p++){
    CHSV temp = rgb2hsv_approximate(pixel[p]);
    temp.v = b;
    pixel[p] = temp;
  }
}

/**
 * Decreases the global brightnessvalue "gBrightness" while keeping it 
 * in the range "MIN_BRIGHTNESS" - "MAX_BRIGHTNESS"
 * takes:
 * - "deltab" change in brightness value in the range 0-255
 */
void decreaseBrightness(uint8_t deltab){
  FastLED.setBrightness((deltab > (FastLED.getBrightness() - MIN_BRIGHTNESS)) 
    ? MIN_BRIGHTNESS 
    : (FastLED.getBrightness() - deltab));
}

/**
 * Increases the global brightnessvalue "gBrightness" while keeping it 
 * in the range "MIN_BRIGHTNESS" - "MAX_BRIGHTNESS"
 * takes:
 * - "deltab" change in brightness value in the range 0-255
 */
void increaseBrightness(uint8_t deltab){
  FastLED.setBrightness((deltab > (MAX_BRIGHTNESS - FastLED.getBrightness())) 
    ? MAX_BRIGHTNESS 
    : (FastLED.getBrightness() + deltab));
}

/**
 * Cycles through the rainbow colors.
 * (increases the hue value by "hstep")
 * takes:
 * - "hstep" the step to take in the hue value
 * - "brightness" a brightness value in the range 0-255
 */
CHSV cycleHSV(uint8_t hstep, uint8_t brightness){
  static uint8_t hue = HUE_RED;
  hue += hstep;
  return CHSV(hue, 255, brightness);
}
/**
 * This function is like 'triwave8', which produces a 
 * symmetrical up-and-down triangle sawtooth waveform, except that this
 * function produces a triangle wave with a faster attack and a slower decay:
 *
 *     / \ 
 *    /     \ 
 *   /         \ 
 *  /             \ 
 *
 */
uint8_t attackDecayWave8(uint8_t i, uint8_t attackSpeed)
{
  uint8_t riseUntil = 255/attackSpeed + 1;
  if( i < riseUntil) {
    return i * attackSpeed;
  } else {
    i -= riseUntil;
    return 255 - (i + (i/(attackSpeed - 1)));
  }
}

/**
 * Set color of sidefacing leds to mach the color of the closest frontfacing led
 */
void adaptSideToFront(void)
{
  pixelsSide[0] = pixelsFront[2];
  pixelsSide[1] = pixelsFront[4];
  pixelsSide[2] = pixelsFront[12];
  pixelsSide[3] = pixelsFront[15];
}


/* ----- BUTTON EVENT CALLBACKS ----- */

void modeHandler(SmartButton *button, SmartButton::Event event, int clickCounter){
  switch(event){
    case SmartButton::Event::HOLD:
      shutdown();
      break;
    case SmartButton::Event::CLICK:
      nextMode();
      break;
    default:
      break;
  }
}

void touch1_Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  modeAllocation[currentMode]->touch1Handler(button, event, clickCounter);
}

void touch2_Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  modeAllocation[currentMode]->touch2Handler(button, event, clickCounter);
}

void touch3_Handler(SmartButton *button, SmartButton::Event event, int clickCounter){
  modeAllocation[currentMode]->touch3Handler(button, event, clickCounter);
}

/*
void increaseBrightness(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
    event == SmartButton::Event::HOLD_REPEAT ||
    event == SmartButton::Event::LONG_HOLD ||
    event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    increaseBrightness(1);
  }
}

void decreaseBrightness(SmartButton *button, SmartButton::Event event, int clickCounter){
  if(event == SmartButton::Event::HOLD ||
    event == SmartButton::Event::HOLD_REPEAT ||
    event == SmartButton::Event::LONG_HOLD ||
    event == SmartButton::Event::LONG_HOLD_REPEAT)
  {
    decreaseBrightness(1);
  }
}*/

void userColorChanger(SmartButton *button, SmartButton::Event event, int clickCounter){
  static uint8_t hue = 0;
  fill_solid(&pixelsSide[0], PIXELS_SIDE_NUM, CHSV(hue, 255, 255));
  fill_solid(&pixelsFront[0], PIXELS_FRONT_NUM, CHSV(hue, 255, 255));
  hue++;
}
